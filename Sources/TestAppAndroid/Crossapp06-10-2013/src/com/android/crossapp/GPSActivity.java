//Name: GPSActivity
//Beschreibung: Dieses Modul greift die Daten des GPS- Sensors ab
//und zeigt diese an.
//Autor: Markus Domin

package com.android.crossapp;

import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;
import android.app.Activity;
import android.content.Context;
import android.view.Menu;
import android.widget.TextView;

public class GPSActivity extends Activity implements LocationListener {
	
	//Textfeldvariablen f�r die Koordinaten
	TextView mLong;
	TextView mLat;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_gps);
		
		
		LocationManager locationManager = (LocationManager) 
				getSystemService(Context.LOCATION_SERVICE);
		
		
		mLong = (TextView)findViewById(R.id.txtLong);
		mLat = (TextView)findViewById(R.id.txtLat);
		 
		//GPS abruf initialiesieren
		locationManager.requestLocationUpdates(  
		LocationManager.GPS_PROVIDER, 5000, 10, this);
	}	
	
	//Auktuelle Position ausgeben
	@Override
    public void onLocationChanged(Location loc) {		
		mLat.setText(loc.getLatitude()+"");
		mLong.setText(loc.getLongitude()+"");
    }
	
	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.g, menu);
		return true;
	}

    @Override
    public void onProviderDisabled(String provider) {}

    @Override
    public void onProviderEnabled(String provider) {}

    @Override
    public void onStatusChanged(String provider, int status, Bundle extras) {}

}
