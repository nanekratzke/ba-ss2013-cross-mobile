package com.android.crossapp;

import android.R.id;
import android.os.Bundle;
import android.app.Activity;
import android.content.Intent;
import android.view.Menu;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;

public class MainActivity extends Activity {

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
		
		Button kamera =(Button)findViewById(R.id.MainbuttonKamera);
		Button gps =(Button)findViewById(R.id.MainbuttonGPS);
		Button aktivitaet =(Button)findViewById(R.id.MainbuttonAktivitaet);
		Button kompass =(Button)findViewById(R.id.MainbuttonKompass);
		
		final Intent kameraview = new Intent(getApplicationContext(), KameraActivity.class);
		final Intent gpsview = new Intent(getApplicationContext(), GPSActivity.class);
		final Intent aktivitaetview = new Intent(getApplicationContext(), AktivitaetActivity.class);
		final Intent kompassview = new Intent(getApplicationContext(), KompassActivity.class);
		
		
		kamera.setOnClickListener(new OnClickListener() {			
			@Override
			public void onClick(View v) {
				startActivity(kameraview);				
			}
		});
		
		gps.setOnClickListener(new OnClickListener() {			
			@Override
			public void onClick(View v) {
				startActivity(gpsview);
			}
		});
		
		aktivitaet.setOnClickListener(new OnClickListener() {			
			@Override
			public void onClick(View v) {
				startActivity(aktivitaetview);
			}
		});
		
		kompass.setOnClickListener(new OnClickListener() {			
			@Override
			public void onClick(View v) {
				startActivity(kompassview);
			}
		});
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.main, menu);
		return true;
	}

}
