//Name: KompassActivity
//Beschreibung: Dieses Modul greift die Daten des Kompass- Sensors ab
//und dreht anhand dessen eine Kompassicon.
//Autor: Markus Domin

package com.android.crossapp;
import android.app.Activity;
import android.content.Context;
import android.graphics.*;
import android.graphics.Paint.Style;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorListener;
import android.hardware.SensorManager;
import android.os.Bundle;
import android.util.Config;
import android.util.Log;
import android.view.Menu;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.ImageView;


public class KompassActivity extends Activity implements SensorEventListener  {
	
	  //Hilfsvariable f�r die Ausrichtung nach Norden
	  float mNordview; 
	  float[] mGravity;
	  float[] mGeomagnetic;
	  //Variable f�r das Kompassicon
	  ImageView mImageView;
	  
	  private SensorManager mSensorManager;
	  Sensor accelerometer;
	  Sensor magnetometer;
	 
	  protected void onCreate(Bundle savedInstanceState) {
	    super.onCreate(savedInstanceState);
	    setContentView(R.layout.activity_kompass);
	    
	    mImageView = (ImageView)findViewById(R.id.imageView1);
	    
	    //Sensoren initialiesiren
	    mSensorManager = (SensorManager)getSystemService(SENSOR_SERVICE);
	    accelerometer = mSensorManager.getDefaultSensor(Sensor.TYPE_ACCELEROMETER);
	    magnetometer = mSensorManager.getDefaultSensor(Sensor.TYPE_MAGNETIC_FIELD);
	    
	    //Eventlistner registrieren
	    mSensorManager.registerListener(this, accelerometer, SensorManager.SENSOR_DELAY_UI);
	    mSensorManager.registerListener(this, magnetometer, SensorManager.SENSOR_DELAY_UI);	  
	    
	  }
	 //Auf das Event nach dem Standby reagieren
	  protected void onResume() {
	    super.onResume();
	    mSensorManager.registerListener(this, accelerometer, SensorManager.SENSOR_DELAY_UI);
	    mSensorManager.registerListener(this, magnetometer, SensorManager.SENSOR_DELAY_UI);
	  }
	 
	  //Auf Standby reagieren
	  protected void onPause() {
	    super.onPause();
	    mSensorManager.unregisterListener(this);
	  }	 
	  
	  public void onAccuracyChanged(Sensor sensor, int accuracy) {  }	 

	  //Event listner f�r die Sensoren
	  public void onSensorChanged(SensorEvent event) {
	    if (event.sensor.getType() == Sensor.TYPE_ACCELEROMETER)
	      mGravity = event.values;
	    if (event.sensor.getType() == Sensor.TYPE_MAGNETIC_FIELD)
	      mGeomagnetic = event.values;
	    if (mGravity != null && mGeomagnetic != null) {
	      float R[] = new float[9];
	      float I[] = new float[9];
	      //Umrechnen der Sensorwerten in das Koordinatensystem der Welt
	      boolean tFinisch = SensorManager.getRotationMatrix(R, I, mGravity, mGeomagnetic);
	      if (tFinisch) {
	        float tOrientation[] = new float[3];
	        SensorManager.getOrientation(R, tOrientation);
	        //Auslesen des Azimut(abweichung von Norden)
	        mNordview = tOrientation[0];
	        //Umrechnen in einen Rotationswinkel
	        mImageView.setRotation(-mNordview*360/(2*(float)Math.PI));
	      }
	    }
	   
	  }
	}	

