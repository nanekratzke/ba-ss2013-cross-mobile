//Name: KameraActivity
//Beschreibung: Dieses Modul nimmt ein Bild mit der Kamera auf 
//und zeigt dieses an.
//Autor: Markus Domin

package com.android.crossapp;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;

import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.provider.MediaStore.Images.Media;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.hardware.Camera;
import android.hardware.Camera.CameraInfo;
import android.hardware.Camera.PictureCallback;
import android.util.Log;
import android.view.Menu;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.Toast;

public class KameraActivity extends Activity {
	

	private ImageView mImageView;
	private int CAMERA_FOTO=1;
	private File mOutputDir;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_kamera);
		
		final Intent netzwerkview = new Intent(getApplicationContext(), NetzwerkActivity.class);
		final Intent bluetoothview = new Intent(getApplicationContext(), BluetoothActivity.class);
		
		
		Button netzwerk =(Button)findViewById(R.id.kameraButtonNetzwerk);
		Button bluetooth =(Button)findViewById(R.id.kameraButtonBluetooth);
		Button aufnehmen =(Button)findViewById(R.id.kameraButtonAufnehmen);
		mImageView =(ImageView)findViewById(R.id.imageView1);		
	    		
		//Buttonlistner der zur aufnahme eines Bildes gestartet wird
		aufnehmen.setOnClickListener(new OnClickListener() {			
			@Override
			public void onClick(View v) {				
				Intent tPictureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
				//Speicherort festlegen
				File dir= Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DCIM);
				mOutputDir = new File(dir, "CameraContentDemo.jpeg");
				tPictureIntent.putExtra(MediaStore.EXTRA_OUTPUT, Uri.fromFile(mOutputDir));
				//Aufnahme Dialog starten
				startActivityForResult(tPictureIntent,CAMERA_FOTO);  				
			}
		});
		
		netzwerk.setOnClickListener(new OnClickListener() {			
			@Override
			public void onClick(View v) {
				startActivity(netzwerkview);				
			}
		});
		
		bluetooth.setOnClickListener(new OnClickListener() {			
			@Override
			public void onClick(View v) {
				startActivity(bluetoothview);
			}
		});
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.kamera, menu);
		return true;
	}
	
	//Funktion die ausgef�hrt wird nach dem aufnehmen eines Bildes
	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
	   if(requestCode == CAMERA_FOTO && resultCode == RESULT_OK){
		   Uri turi = Uri.fromFile(mOutputDir);
		   try {
			   	//Bild auf die richtige Gr��e skallieren
		          Bitmap tBmp = Media.getBitmap(getContentResolver(), turi );		          
		          int tScal = (int) ( tBmp.getHeight() * (512.0 / tBmp.getWidth()) );
		          Bitmap tScalBmp = Bitmap.createScaledBitmap(tBmp, 512, tScal, true);		          
		          mImageView.setImageBitmap(tScalBmp);
		   } catch (Exception e) {
		          e.printStackTrace();		        
		   }
	   }		   
	}
}
