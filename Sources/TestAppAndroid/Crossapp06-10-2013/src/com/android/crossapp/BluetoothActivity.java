package com.android.crossapp;

import java.io.InputStream;

import android.net.Uri;
import android.os.Bundle;
import android.app.Activity;
import android.content.Intent;
import android.graphics.Bitmap;
import android.util.Log;
import android.view.Menu;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;

public class BluetoothActivity extends Activity {

	protected static final int REQUEST_CODE = 1;
	private Bitmap bitmap;
	private String mImagePath;
	private String mImageData;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_bluetooth);
		
		Button versenden =(Button)findViewById(R.id.BluetoothButtonVersenden);		
		//Buttonlistner der zur auswahl eines Bildes gestartet wird	
		versenden.setOnClickListener(new OnClickListener() {			
			@Override
			public void onClick(View v) {
				Intent intent = new Intent();
			    intent.setType("image/*");
			    intent.setAction(Intent.ACTION_GET_CONTENT);
			    intent.addCategory(Intent.CATEGORY_OPENABLE);
			    //Auswahldialog Starten
			    startActivityForResult(intent, REQUEST_CODE);				
			}
		});		
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.bluetooth, menu);
		return true;
	}
	
	//Auswertung des ausgewählten Bildes und Starte des POST Request
	@Override
	  protected void onActivityResult(int requestCode, int resultCode, Intent data) {
	    InputStream stream = null;
	    if (requestCode == REQUEST_CODE && resultCode == Activity.RESULT_OK)
	      try {
	    	  Uri tImage = data.getData();
	    	  Intent tIntent = new Intent();
	    	  tIntent.setAction(Intent.ACTION_SEND);
	    	  tIntent.setType("text/plain");
	    	  tIntent.putExtra(Intent.EXTRA_STREAM, tImage); 
	    	  //BluetoothDialog starten
	    	  startActivity(tIntent);
	      } catch (Exception e) {
	        e.printStackTrace();
	      } 
	  }

}
