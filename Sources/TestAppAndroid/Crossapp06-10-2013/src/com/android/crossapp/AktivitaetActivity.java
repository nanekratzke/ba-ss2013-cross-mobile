//Name: AktivitaetActivity
//Beschreibung: Dieses Modul greift die Daten des Beschleunigungs- Sensors ab
//und zeigt diese an.
//Autor: Markus Domin

package com.android.crossapp;

import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.os.Bundle;
import android.app.Activity;
import android.content.Context;
import android.view.Menu;
import android.view.ViewGroup.LayoutParams;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;

public class AktivitaetActivity extends Activity implements SensorEventListener {

	private SensorManager mSensorManager;
	private Sensor mAccelerometer;
	private TableLayout mAktivTable;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_aktivitaet);

		mAktivTable = (TableLayout) findViewById(R.id.AktiveTableLayoutWerte);
		// Sensor initialisieren
		mSensorManager = (SensorManager) getSystemService(Context.SENSOR_SERVICE);
		mAccelerometer = mSensorManager
				.getDefaultSensor(Sensor.TYPE_ACCELEROMETER);
		mSensorManager.registerListener(this, mAccelerometer,
				SensorManager.SENSOR_DELAY_UI);
	}

	// Sensorwerte auslesen
	public void onSensorChanged(SensorEvent event) {
		if (event.sensor.getType() == Sensor.TYPE_ACCELEROMETER) {
			String werte = event.values[0] + " " + event.values[1] + " "
					+ event.values[2];
			addRow(werte);
		}
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.aktivitaet, menu);
		return true;
	}
	//Sensorwerte der Tabelle hinzufügen
	private void addRow(String data) {
		TableRow row = new TableRow(this);
		TextView textViewData = new TextView(this);
		textViewData.setText(data);
		// textViewData.setBackgroundResource(R.drawable.cell_shape);
		row.addView(textViewData);

		mAktivTable.addView(row, new TableLayout.LayoutParams(
				LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT));
	}

	protected void onResume() {
		super.onResume();
		mSensorManager.registerListener(this, mAccelerometer,
				SensorManager.SENSOR_DELAY_UI);
	}

	protected void onPause() {
		super.onPause();
		mSensorManager.unregisterListener(this);
	}

	public void onAccuracyChanged(Sensor sensor, int accuracy) {
	}

}
