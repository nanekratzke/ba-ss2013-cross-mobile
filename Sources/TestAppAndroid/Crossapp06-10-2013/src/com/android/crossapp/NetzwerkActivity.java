//Name: NetzwerkActivity
//Beschreibung: Dieses Modul �ffnet ein Auswahlfenster f�r Bilder
//und sendet das ausgew�hlte Bild dann per POST Request an einen Server.
//Autor: Markus Domin

package com.android.crossapp;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.URI;

import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.mime.MultipartEntity;
import org.apache.http.entity.mime.content.ByteArrayBody;
import org.apache.http.impl.client.DefaultHttpClient;



import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.provider.MediaStore.MediaColumns;
import android.app.Activity;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.util.Log;
import android.view.Menu;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

public class NetzwerkActivity extends Activity {
	
	private static final int REQUEST_CODE = 1;
	private Bitmap bitmap;
	private String mImagePath;
	private String mImageData;
	private int column_index;
	private EditText urlstring;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_netzwerk);
		
		Button auswahl =(Button)findViewById(R.id.buttonNetzwerkVersenden);		
		//Buttonlistner der zur auswahl eines Bildes gestartet wird	
		urlstring= (EditText)findViewById(R.id.textNetzwerkUrl);	
		urlstring.setText("http://x-y-z.bplaced.net/uploadfile.php");
		
		auswahl.setOnClickListener(new OnClickListener() {			
			@Override
			public void onClick(View v) {
				Intent intent = new Intent();
			    intent.setType("image/*");
			    intent.setAction(Intent.ACTION_GET_CONTENT);
			    intent.addCategory(Intent.CATEGORY_OPENABLE);
			    //Auswahldialog Starten
			    startActivityForResult(intent, REQUEST_CODE);				
			}
		});		
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.netzwerk, menu);
		return true;
	}
	
	//Auswertung des ausgew�hlten Bildes und Starte des POST Request
	@Override
	  protected void onActivityResult(int requestCode, int resultCode, Intent data) {
	    InputStream stream = null;
	    if (requestCode == REQUEST_CODE && resultCode == Activity.RESULT_OK)
	      try {
	    	  Uri tImage = data.getData();
				tImage.getPath();
				//Bild einlesen
				mImagePath = getPath(tImage);
				mImageData.getBytes();
				Log.i("md", "image " + mImageData.toString());
				new PostImage().execute("Hallo");	       
	      } catch (Exception e) {
	        e.printStackTrace();
	      } 
	  }
	
		//ermittel den Pfad zu einer Datei
		public String getPath(Uri uri) {
			String[] tProjection = { MediaColumns.DATA };
			Cursor tCursor = managedQuery(uri, tProjection, null, null, null);
			column_index = tCursor.getColumnIndexOrThrow(MediaColumns.DATA);
			tCursor.moveToFirst();
			mImageData = tCursor.getString(column_index);

			return tCursor.getString(column_index);
		}
		
		//Innere Klasse zum versenden eines Bildes an einen Server per POST Request
		class PostImage extends AsyncTask<String, Void, Void>{

			@Override
			protected Void doInBackground(String... params) {				
				try{
					
					Log.i("imagurl",urlstring.getText().toString());
					URI uri = new URI(urlstring.getText().toString());
					//Request anlegen
					HttpClient httpclient = new DefaultHttpClient();			 
					HttpPost post = new HttpPost(uri);
					
					//Bild einlesen
					File file = new File(mImagePath.toString());					
					int size = (int) file.length();
				    byte[] bytes = new byte[size];
				    try {
				        BufferedInputStream buf = new BufferedInputStream(new FileInputStream(file));
				        buf.read(bytes, 0, bytes.length);
				        buf.close();
				    } catch (Exception e) {
				        // TODO Auto-generated catch block
				        e.printStackTrace();
				    } 	    
				    
				    //Request zusammenstellen
					MultipartEntity entity = new MultipartEntity();	
					ByteArrayBody bab = new ByteArrayBody(bytes, "image2.png");
					entity.addPart("media", bab);
					post.setEntity(entity);
					//Request starten
					HttpResponse response = httpclient.execute(post);	
					
					logoutresponse(response); 	
		       
				}catch(Exception ex){
					Log.i("Md bild", ex.toString());
					
				}
				
				return null;
			}

			private void logoutresponse(HttpResponse response) throws IOException {
				Log.i("MD","Webservices output - " + response.getStatusLine().getStatusCode());			
		        Log.i("MD","Webservices output2 - " +  response.getStatusLine().getReasonPhrase());	        
		        BufferedReader r = new BufferedReader(new InputStreamReader(response.getEntity().getContent()));
		        StringBuilder total = new StringBuilder();
		        String line = null;
		        while ((line = r.readLine()) != null) {
		           total.append(line);
		        }
		        Log.i("MD","Webservices output3 #"+line+"#");
			}
			
			
		}

}
