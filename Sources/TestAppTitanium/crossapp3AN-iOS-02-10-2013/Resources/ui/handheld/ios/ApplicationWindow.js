function ApplicationWindow() {
		var MasterView = require('ui/common/MasterView'),
		KompassView =require('ui/common/KompassView'),
		GPSView =require('ui/common/GPSView'),
		KameraView =require('ui/common/KameraView'),
		AktivitaetView =require('ui/common/AktivitaetView');
		NetzwerkView =require('ui/common/NetView');
	
	var self = Ti.UI.createWindow({
		backgroundColor:'#ffffff'
	});
		
	var masterView = new MasterView(),
		kompassView = new KompassView(),
		kameraView = new KameraView(),
		gpsView = new GPSView(),
		aktiviteatView = new AktivitaetView();
		netzwerkView = NetzwerkView();
		
	var masterContainerWindow = Ti.UI.createWindow({
		title:'Menü'
	});
	masterContainerWindow.add(masterView);
	
		//create iOS NavGroup
	var navGroup = Ti.UI.iPhone.createNavigationGroup({
		window:masterContainerWindow
	});
	self.add(navGroup);
	
	var kompassContainerWindow = Ti.UI.createWindow({
		title:'Kompass'
	});
	kompassContainerWindow.add(kompassView);	
	
	var gpsContainerWindow = Ti.UI.createWindow({
		title:'GPS'
	});
	gpsContainerWindow.add(gpsView);
	
	var netzwerkContainerWindow = Ti.UI.createWindow({
		title:'Netzwerk'
	});
	netzwerkContainerWindow.add(netzwerkView);
	
	kameraView.addEventListener('ButtonSelected', function(e) {
		if(e.name =='Netzwerk')	{
			navGroup.open(netzwerkContainerWindow);
		}		
	});
	
	var kameraContainerWindow = Ti.UI.createWindow({
		title:'Kamera'
	});
	kameraContainerWindow.add(kameraView);
	
	var aktiviteatContainerWindow = Ti.UI.createWindow({
		title:'Aktivität'
	});
	aktiviteatContainerWindow.add(aktiviteatView);
	
	masterView.addEventListener('ButtonSelected', function(e) {
		
		if(e.name =='Kompass')	{
			navGroup.open(kompassContainerWindow);
		}else if(e.name =='Kamera')	{
			navGroup.open(kameraContainerWindow);
		}else if(e.name =='GPS')	{
			navGroup.open(gpsContainerWindow);
		}else if(e.name =='Aktiviteat')	{
			navGroup.open(aktiviteatContainerWindow);
		}		
	});
	
	return self;
};

module.exports = ApplicationWindow;
