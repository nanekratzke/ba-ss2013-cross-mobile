function ApplicationWindow() {
	//declare module dependencies
	var MasterView = require('ui/common/MasterView'),
		DetailView = require('ui/common/DetailView'),
		KompassView =require('ui/common/KompassView'),
		GPSView =require('ui/common/GPSView'),
		KameraView =require('ui/common/KameraView'),
		AktivitaetView =require('ui/common/AktivitaetView');
		
	//create object instance
	var self = Ti.UI.createWindow({
		title:'Products',
		exitOnClose:true,
		navBarHidden:false,
		backgroundColor:'#ffffff'
	});
		
	//construct UI
	var masterView = new MasterView();
	self.add(masterView);
	
	
	masterView.addEventListener('ButtonSelected', function(e) {		
		Titanium.API.info(e.name);	
			if(e.name =='Kompass')	{
				
				var kompassView = new KompassView();
				var kompassContainerWindow = Ti.UI.createWindow({
					title:'Kompass',
					navBarHidden:false,
					backgroundColor:'#ffffff'
				});			
				kompassContainerWindow.add(kompassView);		
				kompassContainerWindow.open();
				
			}
			else if(e.name == 'Kamera')
			{
				var kameraView = new KameraView();
				var kameraContainerWindow = Ti.UI.createWindow({
					title:'Kamera',
					navBarHidden:false,
					backgroundColor:'#ffffff'
				});			
				kameraContainerWindow.add(kameraView);		
				kameraContainerWindow.open();
			}else if(e.name =='GPS')
			{
				var gPSView = new GPSView();
				var gPSContainerWindow = Ti.UI.createWindow({
					title:'GPS',
					navBarHidden:false,
					backgroundColor:'#ffffff'
				});			
				gPSContainerWindow.add(gPSView);		
				gPSContainerWindow.open();
			}else if(e.name =='Aktivitaet')
			{
				var aktivitaetView = new AktivitaetView();
				var aktivitaetContainerWindow = Ti.UI.createWindow({
					title:'Aktivitaet',
					navBarHidden:false,
					backgroundColor:'#ffffff'
				});			
				aktivitaetContainerWindow.add(aktivitaetView);		
				aktivitaetContainerWindow.open();
			}
	});
	
	return self;
};

module.exports = ApplicationWindow;
