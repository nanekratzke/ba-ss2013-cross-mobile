//Master View Component Constructor
function MasterView() {
	//create object instance, parasitic subclass of Observable
	var self = Ti.UI.createView({
		backgroundColor:'white',
		layout:'vertical'
	});
		
	var imageView = Ti.UI.createImageView({
       //top:50,
       image:'earth.png',
        width:64,
        height:64
    });
    self.add(imageView);
    
	var buttonKompass= Ti.UI.createButton({
		   title: 'Kompass',
	//	   top: 300,
		   width: 300,
		   height: 70
	});	
	self.add(buttonKompass);
	
	buttonKompass.addEventListener('click',function(e)
	{
		self.fireEvent('ButtonSelected', {
			name: 'Kompass'
		});
	});
	
	var buttonKamera= Ti.UI.createButton({
		   title: 'Kamera',
//		   top: 390,
		   width: 300,
		   height: 70
	});	
	self.add(buttonKamera);
	
	buttonKamera.addEventListener('click',function(e)
	{
		self.fireEvent('ButtonSelected', {
			name: 'Kamera'
		});
	});
	
	var buttonGPS= Ti.UI.createButton({
		   title: 'GPS',
//		   top: 480,
		   width: 300,
		   height: 70
	});	
	self.add(buttonGPS);
	
	buttonGPS.addEventListener('click',function(e)
	{
		self.fireEvent('ButtonSelected', {
			name: 'GPS'
		});
	});
	
	var buttonAktivitaet= Ti.UI.createButton({
		   title: 'Aktivitaet',
//		   top: 570,
		   width: 300,
		   height: 70
	});	
	self.add(buttonAktivitaet);
	
	buttonAktivitaet.addEventListener('click',function(e)
	{
		self.fireEvent('ButtonSelected', {
			name: 'Aktivitaet'
		});
	});
	
	return self;
};

module.exports = MasterView;