//Name: KameraView
//Beschreibung: Dieses Modul nimmt ein Bild mit der Kamera auf 
//und zeigt dieses an.
//Autor: Markus Domin

function KameraView() {
	var self = Ti.UI.createView({    
    layout:'vertical'
	});
	
	var NetView = require('ui/common/NetView');
	var BluetoothView = require('ui/common/BluetoothView');
	
	var lbl = Ti.UI.createLabel({
		text:'Kamera',
	//	top: 10,
		height:'auto',
		width:'auto',
		color:'#000'
	});
	self.add(lbl);
	
	var imageView = Ti.UI.createImageView({
 //      top:40,
        width:200,
        height:200
    });
    self.add(imageView);
    
    
    var buttonCapture = Ti.UI.createButton({
		   title: 'Kamera',
	//	   top: 270,
		   width: 300,
		   height: 70
	});	
	self.add(buttonCapture);
	
	//Button event zum beginn der Aufnahmen
	buttonCapture.addEventListener('click',function(e)
	{	
		//Kamera Aufnahmedialog anzeigen
		Titanium.Media.showCamera({
			//aufgenommenes Bild anzeigen 
		    success:function(event)
		    {
		        var cropRect = event.cropRect;
		        var image = event.media; 
		       imageView.image=image;
		    },
		    cancel:function()
		    { 
		    },
		    //Fehler abfangen
		    error:function(error)
		    {
		        // Fehler Nachricht erstellen
		        var tError = Titanium.UI.createAlertDialog({title:'Camera'});		    
		        if (error.code == Titanium.Media.NO_CAMERA)
		        {
		            tError.setMessage('Das Gerät hat keine möglichkeit ein Bild aufzunehmen');
		        }
		        else
		        {
		            tError.setMessage('Unbekanter Fehler ' + error.code);
		        }
		        tError.show();
		    },
		    saveToPhotoGallery:true,
		    allowEditing:true
		});
	});
	
	var lblShare = Ti.UI.createLabel({
		text:'Bild teilen',
//		top: 380,
		height:'auto',
		width:'auto',
		color:'#000'
	});
	self.add(lblShare);
	
	 var buttonNetzwerk = Ti.UI.createButton({
		   title: 'Netzwerk',
//		   top: 420,
		   width: 300,
		   height: 70
	});	
	self.add(buttonNetzwerk);
	
	
	
	
	
	var buttonBluetooth = Ti.UI.createButton({
		   title: 'Bluetooth',
//		   top: 530,
		   width: 300,
		   height: 70
	});	
	self.add(buttonBluetooth);
	
	
	
	
	var osname = Ti.Platform.osname;
	
	if (osname == 'iphone') {
		buttonNetzwerk.addEventListener('click',function(e)
		{
			self.fireEvent('ButtonSelected', {
				name: 'Netzwerk'
			});
		});
	}
	else 
	{
		buttonNetzwerk.addEventListener('click',function(e)
		{
			var netView = new NetView();
					var netContainerWindow = Ti.UI.createWindow({
						title:'Netzwerk',
						navBarHidden:false,
						backgroundColor:'#ffffff'
					});			
					netContainerWindow.add(netView);		
					netContainerWindow.open();
		});
		
		buttonBluetooth.addEventListener('click',function(e)
		{
			var bluetoothView = new BluetoothView();
					var bluetoothContainerWindow = Ti.UI.createWindow({
						title:'Bluetooth',
						navBarHidden:false,
						backgroundColor:'#ffffff'
					});			
					bluetoothContainerWindow.add(bluetoothView);		
					bluetoothContainerWindow.open();
		});
	}
	
	
	return self;
};

module.exports = KameraView;
