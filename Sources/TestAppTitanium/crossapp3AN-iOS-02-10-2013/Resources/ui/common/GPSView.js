//Name: GPSView
//Beschreibung: Dieses Modul greift die Daten des GPS- Sensors ab
//und zeigt diese an.
//Autor: Markus Domin

function GPSView() {
	var self = Ti.UI.createView({    
    layout:'vertical'
	});
	
	var lbl = Ti.UI.createLabel({
		text:'GPS',
	//	top: 10,
		height:'auto',
		width:'auto',
		color:'#000'
	});
	self.add(lbl);
	
	var lblLong = Ti.UI.createLabel({
		text:'Longitude',
	//	top: 40,
		height:'auto',
		width:'auto',
		color:'#000'
	});
	self.add(lblLong);
	
	var lblLongvalue = Ti.UI.createLabel({
		text:'',
	//	top: 70,
		height:'auto',
		width:'auto',
		color:'#000'
	});
	self.add(lblLongvalue);
	
	var lblLat = Ti.UI.createLabel({
		text:'Latitude',
	//	top: 100,
		height:'auto',
		width:'auto',
		color:'#000'
	});
	self.add(lblLat);
	
	var lblLatvalue = Ti.UI.createLabel({
		text:'',
	//	top: 130,
		height:'auto',
		width:'auto',
		color:'#000'
	});
	self.add(lblLatvalue);
	
	
	
	
	Titanium.Geolocation.purpose = "GPS user coordinates";
	
	Titanium.Geolocation.distanceFilter = 10; 
	
	var buttonGetGPS = Ti.UI.createButton({
		   title: 'GPS Aktuallisieren',
	//	   top: 160,
		   width: 300,
		   height: 70
	});	
	self.add(buttonGetGPS);
	
	//Button event zum beginn der Localisierung
	buttonGetGPS.addEventListener('click',function(e)
	{	
		//Auslesen des GPS-Sensor starten
		Titanium.Geolocation.getCurrentPosition(function(e)
		{
			//Fehler abfangen
			if (e.error)
			{	                
	                return;
			}	 
			//GPS Koordinaten anzeigen
			lblLongvalue.text = e.coords.longitude;
			lblLatvalue.text = e.coords.latitude;	            
		});
	});
	
	
	return self;
};

module.exports = GPSView;
