//Name: AktivitaetView
//Beschreibung: Dieses Modul greift die Daten des Beschleunigungs- Sensors ab
//und zeigt diese an.
//Autor: Markus Domin

function AktivitaetView() {
	var self = Ti.UI.createView({    
    layout:'vertical'
	});
	
	var lbl = Ti.UI.createLabel({
		text:'Aktivität',
		//top: 10,
		height:'auto',
		width:'auto',
		color:'#000'
	});
	self.add(lbl);	
	
	var tableView = Titanium.UI.createTableView({
//		top: 40,
		height:300,
		width:'auto',
  		data:[]
	});
	self.add(tableView);
	
	var buttonGetAktiv = Ti.UI.createButton({
		   title: 'Start Aktivität',
		//   top: 300,
		   width: 300,
		   height: 70
	});	
	self.add(buttonGetAktiv);
	
	//Auslesen der Beschleunigungswerte und eintragen in eine Tabelle
	var accelerometerCallback = function(e) {	  
	   var row = Ti.UI.createTableViewRow({
        	title:'x: ' + e.x +' y: ' + e.y+' z: ' + e.z
      	});
      tableView.appendRow(row);
	};
	
	//Biginne mit dem auslesen des Beschleunigungssensordaten
	buttonGetAktiv.addEventListener('click',function(e)
	{
		 if (Ti.Platform.model === 'Simulator' || Ti.Platform.model.indexOf('sdk') !== -1 ){
		  alert('Der Beschleunigungssensor funktioniert nicht im Simulator');
		} else {
		//Eventlistner zum Auslesen hinzufügen
		  Ti.Accelerometer.addEventListener('update', accelerometerCallback);
		  //Auf Standbymodus bei Android reagieren
		  if (Ti.Platform.name === 'android'){
		    Ti.Android.currentActivity.addEventListener('pause', function(e) {
		      Ti.Accelerometer.removeEventListener('update', accelerometerCallback);
		    });
		    Ti.Android.currentActivity.addEventListener('resume', function(e) {
		      Ti.Accelerometer.addEventListener('update', accelerometerCallback);
		    });
		  }
		}		
	});
	
	
	
	return self;
};

module.exports = AktivitaetView;
