//Name: NetzwerkPage
//Beschreibung: Dieses Modul dient dazu aus der Galerie ein Bild auszuwählen 
//und danach an einen Server zu versenden
//und zeigt dieses an.
//Autor: Markus Domin

function NetView() {
	var self = Ti.UI.createView({    
    layout:'vertical'
	});
	
	var lbl = Ti.UI.createLabel({
		text:'Netzwerk',
	//	top: 10,
		height:'auto',
		width:'auto',
		color:'#000'
	});
	self.add(lbl);
	
	var textField = Ti.UI.createTextField({
  		borderStyle: Ti.UI.INPUT_BORDERSTYLE_ROUNDED,
  		color: '#336699',  		
  		width: 250, height: 60
	});
	self.add(textField);
	
	textField.value = 'http://x-y-z.bplaced.net/uploadfile.php';
		
	var buttonSenden = Ti.UI.createButton({
		   title: 'Senden',
	//	   top: 160,
		   width: 300,
		   height: 70
	});	
	self.add(buttonSenden);
	
	//Button event zum beginn der Localisierung
	buttonSenden.addEventListener('click',function(e)
	{	
		Titanium.Media.openPhotoGallery({
    		mediaTypes:[Ti.Media.MEDIA_TYPE_PHOTO],
 
    		success:function(event) {
          		var image = event.media;
          		Titanium.API.info('image url: ' + image.nativePath);
          		
          		var httpclient = Ti.Network.createHTTPClient ( );
				httpclient.open ( "POST", textField.value );
				
				httpclient.setTimeout ( 20000 );
				httpclient.send ( 
				{         
					"CommandType"    : "media",
					"file"           : event.media,
					"name"           : "image1.png"
				});
				
				httpclient.onload = function ( e )
				{
					Ti.API.info ("Bild wurde versendet");
				}
        	}
		});		
	});
	
	
	return self;
};

module.exports = NetView;
