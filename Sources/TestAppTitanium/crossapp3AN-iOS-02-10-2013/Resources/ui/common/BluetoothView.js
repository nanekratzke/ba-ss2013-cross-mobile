//Name: NetzwerkPage
//Beschreibung: Dieses Modul dient dazu aus der Galerie ein Bild auszuwählen 
//und danach per Bluetooth zu versenden
//und zeigt dieses an.
//Autor: Markus Domin

function BluetoothView() {
	var self = Ti.UI.createView({    
    layout:'vertical'
	});
	
	var lbl = Ti.UI.createLabel({
		text:'Bluetooth',
	//	top: 10,
		height:'auto',
		width:'auto',
		color:'#000'
	});
	self.add(lbl);
	
	var tableView = Titanium.UI.createTableView({
//		top: 40,
		height:300,
		width:'auto',
  		data:[]
	});
	self.add(tableView);
	
	var buttonGetDeviceRefresh= Ti.UI.createButton({
		   title: 'Geräte Aktuallisieren',
	//	   top: 350,
		   width: 300,
		   height: 70
	});	
	self.add(buttonGetDeviceRefresh);
	
	buttonGetDeviceRefresh.addEventListener('click',function(e)
	{
		 var row = Ti.UI.createTableViewRow({
        	title:'Hallo'
      	});
      tableView.appendRow(row);
	});
		
	
	return self;
};

module.exports = BluetoothView;
