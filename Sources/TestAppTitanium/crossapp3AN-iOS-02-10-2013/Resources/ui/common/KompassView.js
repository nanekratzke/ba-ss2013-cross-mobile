//Name: KompassView
//Beschreibung: Dieses Modul greift die Daten des Kompass- Sensors ab
//und dreht anhand dessen eine Kompassicon.
//Autor: Markus Domin

function KompassView() {
	var self = Ti.UI.createView({    
    layout:'vertical'
	});
	
	//Überschrift Label erstellen
	var lbl = Ti.UI.createLabel({
		text:'Kompass',
		height:'auto',
		width:'auto',
		color:'#000'
	});
	self.add(lbl);
	
	//Kompassicon einfügen
	var imageView = Ti.UI.createImageView({
       image:'compass.png',
        width:217,
        height:250,
        anchorPoint : {
        x : '50%',
        y : '50%'
    }
    });
    self.add(imageView);
	
	
	if (Ti.Geolocation.locationServicesEnabled) {
    Ti.Geolocation.purpose = 'Get Current Heading';
        //Kompass auslesen
         Ti.Geolocation.addEventListener('heading', function(e) 
        {
            
            //Fehler abfangen
            if (e.error)
            {
                alert('Fehler: ' + e.error);
                return;
            }
            Ti.API.info(e.heading);
            //Wert in einen Winkel umrechnen und den Kompass drehen
 			var tmatrix = Ti.UI.create2DMatrix();
        	tmatrix = tmatrix.rotate(360-e.heading.magneticHeading);
			var  tanimation= Ti.UI.createAnimation({
				transform: tmatrix,
				duration: 1,
			});
			
			imageView.animate(tanimation);
			
        });
	}
	
	return self;
};

module.exports = KompassView;
