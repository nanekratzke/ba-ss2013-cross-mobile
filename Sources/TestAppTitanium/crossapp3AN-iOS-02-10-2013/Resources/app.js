
if (Ti.version < 1.8 ) {
	alert('Titanium Mobile SDK 1.8 oder höher wird benötigt ');	  	
}


(function() {

	var osname = Ti.Platform.osname;
	
	var Window;
	
	if (osname == 'iphone') {
		Window = require('ui/handheld/ios/ApplicationWindow');
	}
	else if (osname == 'android') {
		Window = require('ui/handheld/android/ApplicationWindow');
	}
	else {
		Window = require('ui/handheld/android/ApplicationWindow');
	}
	
	new Window().open();
})();
