-- 
-- Dieses Modul Stellt das Fenster für die Aktivitäten dar.
-- Autor: Markus Domin
-- Version: 1.2
--

module(..., package.seeall)

function new()
	--Initalisierung des Fensters
	local mDisplayGroup = display.newGroup()
	local widget = require( "widget" )
	
	-- Hintergrundbild laden/anzeigen
	local mBackground = display.newImage("background.jpg", true)
	mBackground.x = display.contentWidth / 2
	mBackground.y = display.contentHeight / 2
	mDisplayGroup:insert(mBackground)
    --Anzeige des Titels
	local mTitel = display.newText("Aktivitäten", 0, 0, native.systemFontBold, 16)
	mTitel:setTextColor(0, 0, 0)
	mTitel.x = display.contentWidth / 2
	mTitel.y = 20
	mDisplayGroup:insert(mTitel)	
	display.setStatusBar( display.HiddenStatusBar )	
	
	-- Aktivitätentabelle erstellen
	local list = widget.newTableView{
	width = 320,
	height = 366,
	maskFile = "mask.png"
	}
	list.y = 50	
	mDisplayGroup:insert(list)
	 
	 
	local function onRowRender( event )
	local group = event.view
	local row = event.target
	local id = event.id

	text = display.newText( "" .. id, 0, 0, native.systemFontBold, 16 )
	text:setReferencePoint( display.CenterLeftReferencePoint )
	text.x = 25
	text.y = row.height * 0.5
	text:setTextColor(0, 0, 0)
	group:insert( text )
	end	
	
	-- Funktion zum auslesen der Sensorwerte
	local function getAccelorometer(event)   
	list:insertRow{
		id = "x" .. event.xGravity .. " y" .. event.yGravity .. " z" .. event.zGravity,
		onRender = onRowRender
	}
	end
	
	
	Runtime:addEventListener("accelerometer", getAccelorometer)
	
	-- Home Button einfügen
	local buttonHomeRelease = function( event )	
	Runtime:addEventListener("accelerometer", getAccelorometer)
	mDisplayGroup:removeSelf()	
	end
	-- Button zum auslesen des Sensors einfügen
	local buttonHome = widget.newButton{
	default = "buttonWhite.png",
	over = "buttonWhiteOver.png",
	onRelease = buttonHomeRelease,
	label = "zur\195\188ck",
	font = "Trebuchet-BoldItalic",
	fontSize = 22,
	emboss = true
	}	
	buttonHome.x = 160; buttonHome.y = 450	
	mDisplayGroup:insert(buttonHome)	
	
	function mDisplayGroup:cleanUp()
		mDisplayGroup:removeSelf()
	end	
	return mDisplayGroup
end
