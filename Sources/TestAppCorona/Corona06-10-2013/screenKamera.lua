-- 
-- Dieses Modul Stellt das Fenster f�r die Kameraabfrage dar.
-- Autor: Markus Domin
-- Version: 1.2
--

module(..., package.seeall)

function new()
	--Initalisierung des Fensters
	local currentScreen

	local function loadScreen(newScreen)
		if currentScreen then
			currentScreen:cleanUp()
		end
		currentScreen = require(newScreen).new()
			
		return true
	end
	
	local mDisplayGroup = display.newGroup()
	local widget = require( "widget" )
	
	-- Hintergrundbild laden/anzeigen
	local mBackground = display.newImage("background.jpg", true)
	mBackground.x = display.contentWidth / 2
	mBackground.y = display.contentHeight / 2
	mDisplayGroup:insert(mBackground)
    --Anzeige des Titels
	local mTitel = display.newText("Kamera", 0, 0, native.systemFontBold, 16)
	mTitel:setTextColor(0, 0, 0)
	mTitel.x = display.contentWidth / 2
	mTitel.y = 20
	mDisplayGroup:insert(mTitel)	
	display.setStatusBar( display.HiddenStatusBar )	
	
	
	local buttonHomeRelease = function( event )
	mDisplayGroup:removeSelf()
	end
	
	local buttonHome = widget.newButton{
	default = "buttonWhite.png",
	over = "buttonWhiteOver.png",
	onRelease = buttonHomeRelease,
	label = "zur\195\188ck",
	font = "Trebuchet-BoldItalic",
	fontSize = 22,
	emboss = true
	}	
	buttonHome.x = 160; buttonHome.y = 450	
	mDisplayGroup:insert(buttonHome)
	
	local buttonNetRelease = function( event )	
		loadScreen("screenNet")
	end
	
	local buttonNet = widget.newButton{
	default = "buttonWhite.png",
	over = "buttonWhiteOver.png",
	onRelease = buttonNetRelease,
	label = "Netzwerk",
	font = "Trebuchet-BoldItalic",
	fontSize = 22,
	emboss = true
	}	
	buttonNet.x = 160; buttonNet.y = 350	
	mDisplayGroup:insert(buttonNet)
	
	local buttonBluetoothRelease = function( event )
		local alert = native.showAlert( "Information", "Bluetooth wird nicht unterst\195\188tzt.", { "OK"}) 
	end
	
	local buttonBluetooth = widget.newButton{
	default = "buttonWhite.png",
	over = "buttonWhiteOver.png",
	onRelease = buttonBluetoothRelease,
	label = "Bluetooth",
	font = "Trebuchet-BoldItalic",
	fontSize = 22,
	emboss = true
	}	
	buttonBluetooth.x = 160; buttonBluetooth.y = 400	
	mDisplayGroup:insert(buttonBluetooth)	
	
	-- bild anzeigen
	myImage = display.newImage( "imageleer.png", 10, 20 )
	
		
	-- Die Kamera wird im Simulator nicht unterst�tzt                    
	local isXcodeSimulator = "iPhone Simulator" == system.getInfo("model")
	if (isXcodeSimulator) then
		 local alert = native.showAlert( "Information", "Die Kamera wird im Simulator nicht unterst\195\188tzt.", { "OK"})    
	end

	local onComplete = function(event)
	local photo = event.target
		print( "Foto w,h = " .. photo.width .. "," .. photo.height )
		myImage =photo
	end
	-- �berpr�fen ob eine Kamera vorhanden ist
	if media.hasSource( media.Camera ) then
	   media.show( media.Camera, onComplete )
	else
		local isAndroid = "Android" == system.getInfo("platformName")                                 
		if(isAndroid) then
			local alert = native.showAlert( "Information", "Die Kamera unter Android wird nicht unterst\195\188tzt", { "OK"})    
		else		
	   native.showAlert( "Information", "Dieses Ger\195\164t hat keine Kamera", { "OK" } )
	   end
	end
	
	
	function mDisplayGroup:cleanUp()
		mDisplayGroup:removeSelf()
	end	
	
		
	return mDisplayGroup
end
