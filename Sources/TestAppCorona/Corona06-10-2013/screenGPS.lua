-- 
-- Dieses Modul Stellt das Fenster f�r den GPS dar.
-- Autor: Markus Domin
-- Version: 1.2
--

module(..., package.seeall)

function new()
	--Initalisierung des Fensters
	local mDisplayGroup = display.newGroup()
	local widget = require( "widget" )
	
	-- Hintergrundbild laden/anzeigen
	local mBackground = display.newImage("background.jpg", true)
	mBackground.x = display.contentWidth / 2
	mBackground.y = display.contentHeight / 2
	mDisplayGroup:insert(mBackground)
    --Anzeige des Titels
	local mTitel = display.newText("GPS", 0, 0, native.systemFontBold, 16)
	mTitel:setTextColor(0, 0, 0)
	mTitel.x = display.contentWidth / 2
	mTitel.y = 20
	mDisplayGroup:insert(mTitel)	
	display.setStatusBar( display.HiddenStatusBar )
	local ui = require("ui")	

	-- Textfelder f�r die Koordinaten einf�gen
	local txtLatitude = display.newText( "--", 0, 0, native.systemFontBold, 16 )
	txtLatitude.x = display.contentWidth / 2
	txtLatitude.y = 125 
	txtLatitude:setTextColor( 0,0,0)
	mDisplayGroup:insert(txtLatitude)

	local txtLongitude = display.newText( "--", 0, 0, native.systemFontBold, 16)
	txtLongitude.x = display.contentWidth / 2
	txtLongitude.y = 160
	txtLongitude:setTextColor( 0,0,0)
	mDisplayGroup:insert(txtLongitude)
	
	-- Home Button einf�gen
	local buttonHomeRelease = function( event )	
	Runtime:removeEventListener( "location", mGetGps )
	mDisplayGroup:removeSelf()
	--txtLongitude.text = "Hallo"
	end
	
	local buttonHome = widget.newButton{
	default = "buttonWhite.png",
	over = "buttonWhiteOver.png",
	onRelease = buttonHomeRelease,
	label = "zur\195\188ck",
	font = "Trebuchet-BoldItalic",
	fontSize = 22,
	emboss = true
	}	
	buttonHome.x = 160; buttonHome.y = 450	
	mDisplayGroup:insert(buttonHome)
	
	
	-- Event zum abfragen der Koordinaten erstellen
	local mGetGps = function( event )	
		native.showAlert("gps event", event.errorMessage, {"OK"})	
		if event.errorCode then
			native.showAlert( "Fehler bei abfragen der GPS Position", event.errorMessage, {"OK"} )			
		else		
			txtLatitudet.text = string.format( '%.4f', event.latitude )				
			txtLongitude.text = string.format( '%.4f', event.longitude )					

		end
	end
	-- Event zum abfragen der Koordinaten aktivieren
	Runtime:addEventListener( "location", mGetGps )
	

	function mDisplayGroup:cleanUp()
		mDisplayGroup:removeSelf()
	end
	
	return mDisplayGroup
end
