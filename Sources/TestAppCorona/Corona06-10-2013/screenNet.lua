-- 
-- Dieses Modul Stellt das Fenster f�r den Bild upload an den Server dar.
-- Autor: Markus Domin
-- Version: 1.2
--

module(..., package.seeall)

function new()
	--Initalisierung des Fensters
	local mDisplayGroup = display.newGroup()
	local widget = require( "widget" )
	
	-- Hintergrundbild laden/anzeigen
	local mBackground = display.newImage("background.jpg", true)
	mBackground.x = display.contentWidth / 2
	mBackground.y = display.contentHeight / 2
	mDisplayGroup:insert(mBackground)
    --Anzeige des Titels
	local mTitel = display.newText("Netzwerk", 0, 0, native.systemFontBold, 16)
	mTitel:setTextColor(0, 0, 0)
	mTitel.x = display.contentWidth / 2
	mTitel.y = 20
	mDisplayGroup:insert(mTitel)
	
	local photo

	-- Eingabefeld hinzuf�gen
	local textfield = native.newTextField( 0, 100, display.contentWidth, 70 )
	textfield.text = "http://x-y-z.bplaced.net/uploadfile.php/"
	
	-- Bild hochladen zum Server
	local MultipartFormData = require("class_MultipartFormData")
 
	local multipart = MultipartFormData.new()
	
	 
	local params = {}
	params.body = multipart:getBody()
	params.headers = multipart:getHeaders()
	 
	local function networkListener( event )
			if ( event.isError ) then
					print( "Netzwerk Fehler!")
			else
					print ( "RESPONSE: " .. event.response )
			end
	end	 
	
	
	local onComplete = function(event)
		photo = event.target
		
		local photoGroup = display.newGroup()  
		photoGroup:insert(photo)
	 
		local tmpDir = system.TemporaryDirectory
		display.save(photoGroup, "image.jpg", tmpDiry)		
		
		multipart:addField("media","image1")
		multipart:addFile("media", system.pathForFile( "image.jpg", system.TemporaryDirectory ), "image/jpeg", "image1.jpg")
		network.request( textfield.text , "POST", networkListener, params)
		
		print( "Foto wurde hochgeladen" )
	end
	
	-- Hochlade Button einf�gen
	local buttonUploadRelease = function( event )	
		if media.hasSource( media.PhotoLibrary ) then
		media.show( media.PhotoLibrary, onComplete )
		else				
		   native.showAlert( "Information", "Dieses Ger\195\164t hat keine Galerie", { "OK" } )		  
		end		
	end
	
	local buttonUpload = widget.newButton{
		default = "buttonWhite.png",
		over = "buttonWhiteOver.png",
		onRelease = buttonUploadRelease,
		label = "Bild hochladen",
		font = "Trebuchet-BoldItalic",
		fontSize = 22,
		emboss = true
	}

	buttonUpload.x = 160; buttonUpload.y = 400
	mDisplayGroup:insert(buttonUpload);

	
	
	-- Home Button einf�gen
	local buttonHomeRelease = function( event )	
	mDisplayGroup:removeSelf()
	end
	
	local buttonHome = widget.newButton{
	default = "buttonWhite.png",
	over = "buttonWhiteOver.png",
	onRelease = buttonHomeRelease,
	label = "zur\195\188ck",
	font = "Trebuchet-BoldItalic",
	fontSize = 22,
	emboss = true
	}	
	buttonHome.x = 160; buttonHome.y = 450	
	mDisplayGroup:insert(buttonHome)
	
	-- Fenster l�schen
	function mDisplayGroup:cleanUp()
		mDisplayGroup:removeSelf()
	end	
	
	return mDisplayGroup
end
