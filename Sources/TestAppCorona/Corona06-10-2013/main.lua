-- Dieses Modul Stellt das Hauptmen� dar
-- Autor: Markus Domin

local widget = require( "widget" )

local currentScreen

local function loadScreen(newScreen)
	if currentScreen then
		currentScreen:cleanUp()
	end
	currentScreen = require(newScreen).new()
		
	return true
end

-- Hintergrundbild einf�gen
local mBackground = display.newImage("background.jpg", true)
mBackground.x = display.contentWidth / 2
mBackground.y = display.contentHeight / 2

-- Symbol der Erde einf�gen
local earth = display.newImageRect("earth.png",150,150)
earth.x=display.contentWidth / 2
earth.y=50

-- Buttonevents festlegen
local buttonKompassRelease = function( event )
	loadScreen("screenKompass")
end

local buttonGPSRelease = function( event )
	loadScreen("screenGPS")
end

local buttonKameraRelease = function( event )
	loadScreen("screenKamera")
end

local buttonAktivitaetRelease = function( event )
	loadScreen("screenAktivitaet")
end

-- Button einf�gen
local buttonKompass = widget.newButton{
	default = "buttonWhite.png",
	over = "buttonWhiteOver.png",
	onRelease = buttonKompassRelease,
	label = "Kompass",
	font = "Trebuchet-BoldItalic",
	fontSize = 22,
	emboss = true
}

local buttonGPS = widget.newButton{
	default = "buttonWhite.png",
	over = "buttonWhiteOver.png",
	onRelease = buttonGPSRelease,
	label = "GPS",
	font = "Trebuchet-BoldItalic",
	fontSize = 22,
	emboss = true
}

local buttonKamera = widget.newButton{
	default = "buttonWhite.png",
	over = "buttonWhiteOver.png",
	onRelease = buttonKameraRelease,
	label = "Kamera",
	font = "Trebuchet-BoldItalic",
	fontSize = 22,
	emboss = true
}

local buttonAktivitaet = widget.newButton{
	default = "buttonWhite.png",
	over = "buttonWhiteOver.png",
	onRelease = buttonAktivitaetRelease,
	label = "Aktivit\195\164t",
	font = "Trebuchet-BoldItalic",
	fontSize = 22,
	emboss = true
}
-- Positionen bestimmen
buttonKompass.x = 160; buttonKompass.y = 160
buttonGPS.x = 160; buttonGPS.y = 240
buttonKamera.x = 160; buttonKamera.y = 320
buttonAktivitaet.x = 160; buttonAktivitaet.y = 400
