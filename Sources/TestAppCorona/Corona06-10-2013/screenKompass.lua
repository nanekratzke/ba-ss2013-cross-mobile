-- 
-- Dieses Modul Stellt das Fenster f�r den Kompass dar.
-- Autor: Markus Domin
-- Version: 1.2
--

module(..., package.seeall)

function new()
	--Initalisierung des Fensters
	local mDisplayGroup = display.newGroup()
	local widget = require( "widget" )
	
	-- Hintergrundbild laden/anzeigen
	local mBackground = display.newImage("background.jpg", true)
	mBackground.x = display.contentWidth / 2
	mBackground.y = display.contentHeight / 2
	mDisplayGroup:insert(mBackground)
    --Anzeige des Titels
	local mTitel = display.newText("Kompass", 0, 0, native.systemFontBold, 16)
	mTitel:setTextColor(0, 0, 0)
	mTitel.x = display.contentWidth / 2
	mTitel.y = 20
	mDisplayGroup:insert(mTitel)	
	
	local ui = require("ui")	
	-- Kompassbild laden/anzeigen
	local mCompassImage = display.newImage("compass.png", 40, 150)
	
	-- Home Button einf�gen
	local buttonHomeRelease = function( event )	
	Runtime:removeEventListener( "location", tInitNorth )
	Runtime:removeEventListener( "heading", updateCompass )
	Runtime:removeEventListener( "enterFrame", animatemCompassImage )	
	mDisplayGroup:removeSelf()
	end
	
	local buttonHome = widget.newButton{
	default = "buttonWhite.png",
	over = "buttonWhiteOver.png",
	onRelease = buttonHomeRelease,
	label = "zur\195\188ck",
	font = "Trebuchet-BoldItalic",
	fontSize = 22,
	emboss = true
	}	
	buttonHome.x = 160; buttonHome.y = 450	
	mDisplayGroup:insert(buttonHome)
	
	mDisplayGroup:insert(mCompassImage)
	local mDestinationAngel = 0

	local updateCompass = function( event )		
			
		-- Android unterst�tzt den geographischen Kompass nicht daher wird
		-- der magnetisch Kompass genutzt
		if system.getInfo( "platformName" ) ~= "Android" then
                mDestinationAngel = -event.geographic
        else
                mDestinationAngel = -event.magnetic
        end
		
	end

	local animateCompassImage = function( event )
		local mCurrentAngel = mCompassImage.rotation
		local tDeltaAngel = mDestinationAngel - mCurrentAngel
		
		if math.abs( tDeltaAngel ) >= 180 then
			if tDeltaAngel < -180 then
				tDeltaAngel = tDeltaAngel + 360
			elseif tDeltaAngel > 180 then
				tDeltaAngel = tDeltaAngel - 360
			end
		end
		mCompassImage.rotation = mCurrentAngel + tDeltaAngel*0.3
	end
	
	-- Start event zum einstellen des Kompass auf den Norden
	local mCalibrationNorth = false
	local tInitNorth = function( event )
		if ( not mCalibrationNorth ) then
			mCalibrationNorth = true			
			local tInitNorthFinish = function( event )
				Runtime:removeEventListener( "location", tInitNorth )
			end
			timer.performWithDelay( 1000, tInitNorthFinish )
		end
	end
	-- Start Event zum einstellen des Kompass hinzu f�gen
	Runtime:addEventListener( "location", tInitNorth )

	-- Kompass event zum abfragen der Daten hinzuf�gen
	Runtime:addEventListener( "heading", updateCompass )

	-- Kompass animation event hinzuf�gen
	Runtime:addEventListener( "enterFrame", animateCompassImage )
	
	-- Fenster l�schen
	function mDisplayGroup:cleanUp()
		mDisplayGroup:removeSelf()
	end	
	
	return mDisplayGroup
end
