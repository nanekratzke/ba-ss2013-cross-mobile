﻿//Name: KameraPage
//Beschreibung: Dieses Modul nimmt ein Bild mit der Kamera auf 
//und zeigt dieses an.
//Autor: Markus Domin

using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Navigation;
using Microsoft.Phone.Controls;
using Microsoft.Phone.Shell;
using Microsoft.Phone.Tasks;
using System.Windows.Media.Imaging;

namespace PhoneAppCross
{
    public partial class KameraPage : PhoneApplicationPage
    {
        public KameraPage()
        {
            InitializeComponent();
        }

        private void btnNetzwerk_Click(object sender, RoutedEventArgs e)
        {
            NavigationService.Navigate(new Uri("/NetzwerkPage.xaml", UriKind.Relative));
        }

        private void btnBluetooth_Click(object sender, RoutedEventArgs e)
        {
            NavigationService.Navigate(new Uri("/BluetoothPage.xaml", UriKind.Relative));
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            //Kamera Dialog initalisieren
			CameraCaptureTask task = new CameraCaptureTask();
            task.Completed += delegate(object sendern, PhotoResult photoresult)
            {
				//Bild anzeigen
                BitmapImage bitmapimage = new BitmapImage();
                bitmapimage.SetSource(photoresult.ChosenPhoto);
                Imagebox.Source = bitmapimage;
            };
			//Dialog öffnen
            task.Show(); 
        }
    }
}