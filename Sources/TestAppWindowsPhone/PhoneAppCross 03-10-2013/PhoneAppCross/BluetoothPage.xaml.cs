﻿//Name: NetzwerkPage
//Beschreibung: Dieses Modul dient dazu aus der Galerie ein Bild auszuwählen 
//und danach per Bluetooth zu versenden
//Autor: Markus Domin

using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Navigation;
using Microsoft.Phone.Controls;
using Microsoft.Phone.Shell;
using Microsoft.Phone.Tasks;
using System.Windows.Media.Imaging;

namespace PhoneAppCross
{
    public partial class BluetoothPage : PhoneApplicationPage
    {
        public BluetoothPage()
        {
            InitializeComponent();
        }

        private void senden_Click(object sender, RoutedEventArgs e)
        {
            //Dialog zum auswählen des Bildes aus der Galerie
            PhotoChooserTask task = new PhotoChooserTask();
            task.Completed += delegate(object sendern, PhotoResult photoresult)
            {
				//Ist ein Foto ausgewählt kann es versendet werden
                sendImage(photoresult.OriginalFileName);
            };
            task.Show();
        }
        private void sendImage(string pfad)
        {
            //Dialog zum versenden starten
			ShareMediaTask sharetask = new ShareMediaTask();
            sharetask.FilePath = pfad;
            sharetask.Show();
        }
    }
}