﻿//Name: KompassPage
//Beschreibung: Dieses Modul greift die Daten des Kompass- Sensors ab
//und dreht anhand dessen eine Kompassicon.
//Autor: Markus Domin

using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Navigation;
using Microsoft.Phone.Controls;
using Microsoft.Phone.Shell;
using Microsoft.Devices.Sensors;
using System.Windows.Threading;
using System.Windows.Media;

namespace PhoneAppCross
{
    public partial class KompassPage : PhoneApplicationPage
    {
        Compass mCompass;
        DispatcherTimer mTimer;
        double mAngle = 0;
        RotateTransform rt;
        public KompassPage()
        {
            InitializeComponent();
            if (Compass.IsSupported)
            {
				//Rotation initialisieren
				rt = new RotateTransform();
				
				//Timer Event der nach einer Bestimmten Zeit den Kompass dreht
				//Timer initialisieren
                mTimer = new DispatcherTimer();
                mTimer.Interval = TimeSpan.FromMilliseconds(30);
                mTimer.Tick += new EventHandler(mTimer_Tick);				
				//Kompass initialisieren
                mCompass = new Compass();
                mCompass.TimeBetweenUpdates = TimeSpan.FromMilliseconds(20);
                mCompass.CurrentValueChanged += mCompass_CurrentValueChanged;
                try
                {
					//Kompass Starten
                    mCompass.Start();
                    mTimer.Start();
                }
                catch (Exception ex)
                {
                    MessageBox.Show("Fehler beim Starten des Kompass.");
                }
            }
            else
            {
                MessageBox.Show("Das Gerät unterstützt keinen Kompass");
            }
        }

        void mCompass_CurrentValueChanged(object sender, SensorReadingEventArgs<CompassReading> e)
        {
            //Drehwinkel berechnen
            mAngle = -e.SensorReading.MagneticHeading;
        }

        private void mTimer_Tick(object sender, EventArgs e)
        {
            //Kompass drehen
            rt.Angle = mAngle;
            imageCompass.RenderTransform = rt;
        }
    }
}