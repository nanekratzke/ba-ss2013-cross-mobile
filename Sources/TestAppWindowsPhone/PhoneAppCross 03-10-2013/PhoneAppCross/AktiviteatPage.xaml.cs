﻿//Name: AktivitaetView
//Beschreibung: Dieses Modul greift die Daten des Beschleunigungs- Sensors ab
//und zeigt diese an.
//Autor: Markus Domin
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Navigation;
using Microsoft.Phone.Controls;
using Microsoft.Phone.Shell;
using Microsoft.Devices.Sensors;
using System.Collections.ObjectModel;

namespace PhoneAppCross
{
    public partial class AktiviteatPage : PhoneApplicationPage
    {
        Accelerometer accelerometer;
        ObservableCollection<TextBlock> listPickerItems;
        public AktiviteatPage()
        {
            InitializeComponent();
			
			listPickerItems = new ObservableCollection<TextBlock>();
            this.ListboxData.ItemsSource = listPickerItems;
			
			//Sensor initialisieren
            accelerometer = new Accelerometer();
            accelerometer.TimeBetweenUpdates = TimeSpan.FromMilliseconds(500);
            accelerometer.CurrentValueChanged += accelerometer_CurrentValueChanged;
            accelerometer.Start();            

        }
		//Funktion zum auslesen der Sensorwerte
        void accelerometer_CurrentValueChanged(object sender, SensorReadingEventArgs<AccelerometerReading> e)
        {
            this.Dispatcher.BeginInvoke(new Action<string>(addValue), e.SensorReading.Acceleration.X + " " +
                e.SensorReading.Acceleration.Y + " " +
                e.SensorReading.Acceleration.Z);
        }
		//werte in die Tabelle hinzufügen
        private void addValue(string value)
        {
            TextBlock textblock = new TextBlock();
            textblock.Text = value;
            listPickerItems.Add(textblock);
        }

        private void btnStop_Click(object sender, RoutedEventArgs e)
        {
            listPickerItems.Clear();
        }
    }
}