﻿
//Name: NetzwerkPage
//Beschreibung: Dieses Modul dient dazu aus der Galerie ein Bild auszuwählen 
//und danach an einen Server zu versenden.
//Autor: Markus Domin
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Navigation;
using Microsoft.Phone.Controls;
using Microsoft.Phone.Shell;
using Microsoft.Phone.Tasks;
using RestSharp;
using System.IO;

namespace PhoneAppCross
{
    public partial class NetzwerkPage : PhoneApplicationPage
    {
        public NetzwerkPage()
        {
            InitializeComponent();

        }

        private void uploadButton_Click(object sender, RoutedEventArgs e)
        {
            //Dialog zum auswählen des Bildes aus der Galerie
            PhotoChooserTask task = new PhotoChooserTask();
            task.Completed += delegate(object sendern, PhotoResult photoresult)
            {
				//Netzwerk upload initialisieren
                var client = new RestClient(texturl.Text);
                var request = new RestRequest("", Method.POST);
                request.AddFile("media", ReadBytes(photoresult.ChosenPhoto), "image2.png");
                //upload starten
                client.ExecuteAsync(request, r =>
                {                   

                });
            };
            task.Show();            
        }
		// Dient dazu das Bild vollständig einzulesen
        public static byte[] ReadBytes(Stream stream)
        {
            using (var memoryStream = new MemoryStream())
            {
                stream.CopyTo(memoryStream);
                return memoryStream.ToArray();
            }
        }
    }
}