﻿//Name: GPSPage
//Beschreibung: Dieses Modul greift die Daten des GPS- Sensors ab
//und zeigt diese an.
//Autor: Markus Domin

using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Navigation;
using Microsoft.Phone.Controls;
using Microsoft.Phone.Shell;
using System.Threading.Tasks;
using Windows.Devices.Geolocation;

namespace PhoneAppCross
{
    public partial class GPSPage : PhoneApplicationPage
    {
        public GPSPage()
        {
            InitializeComponent();
            getGPS();            
        }

        private async Task getGPS()
        {
            //GPS initialisieren
			Geolocator geolocator = new Geolocator();
            geolocator.DesiredAccuracyInMeters = 50;

            try
            {
                //Position auslesen
				Geoposition geoposition = await geolocator.GetGeopositionAsync(
                    maximumAge: TimeSpan.FromMinutes(5),
                    timeout: TimeSpan.FromSeconds(10));
				//Position anzeigen
                txtlatitude.Text = geoposition.Coordinate.Latitude.ToString();
                txtlongitude.Text = geoposition.Coordinate.Longitude.ToString();
            }
            catch (Exception ex)
            {
                MessageBox.Show("Fehler bei lesen der GPS Position."+ex);
            }
        }

        private void ButtonGetGPS_Click(object sender, RoutedEventArgs e)
        {
            getGPS();
        }
    }
}